import { createApp } from 'vue';
import App from './App.vue'
import {initStore} from './store'
import router from './router'
import {VueMasonryPlugin} from 'vue-masonry'
import vueSmoothScroll from 'vue-smooth-scroll'
import { createI18n } from 'vue-i18n';
import i18njson from './i18n/lang.js';

import BackToTop from 'vue-backtotop'
import _ from 'lodash'
// import VueMeta from 'vue-meta'
import deuFooterHeader, { Header, Footer } from '@deu/deu-header-footer';
import { library } from '@fortawesome/fontawesome-svg-core';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import UniversalPiwik from '@piveau/piveau-universal-piwik'
import RuntimeConfiguration from './utils/RuntimeConfigService.js';

const app = createApp(App);

// Vue.prototype.$_ = _
app.config.globalProperties.$_ = _

/* eslint-disable */
// var Promise = require('es6-promise').Promise;
// require('es6-promise').polyfill();

import '../node_modules/bootstrap/scss/bootstrap.scss';
import './assets/img/deulogo.png';
import '@deu/deu-header-footer/dist/deu-header-footer.css';
import '@ecl/preset-ec/dist/styles/ecl-ec.css';

/**********************************************
 *  Integrating the EC component library here *
 **********************************************/
import '@ecl/preset-ec/dist/styles/ecl-ec.css'
import {faCheckCircle, faQuestionCircle, faTimesCircle} from "@fortawesome/free-regular-svg-icons";
import {faDownload, faSearch} from '@fortawesome/free-solid-svg-icons';
library.add(faDownload, faCheckCircle, faQuestionCircle, faTimesCircle, faSearch);
app.component('font-awesome-icon', FontAwesomeIcon);

// app.use(VueEvents);

app.use(vueSmoothScroll);
// app.use(VueI18n);
app.config.productionTip = false;

// Vue-progressbar setup
// const progressBarOptions = {
//   thickness: '5px',
//   autoRevert: false,
//   transition: {
//     speed: '1.0s',
//     opacity: '0.5s',
//     termination: 1000,
//   },
// };
// Vue.use(VueProgressBar, progressBarOptions);

app.use(BackToTop);
app.use(VueMasonryPlugin);
// Vue.use(VModal, { dynamic: true, injectModalsContainer: true })
app.use(RuntimeConfiguration, { debug: true });
// Vue.use(VueMeta)


app.component('deu-header', Header);
app.component('deu-footer', Footer);

// const LOCALE = Vue.prototype.$env.languages.locale;
// const FALLBACKLOCALE = Vue.prototype.$env.languages.fallbackLocale;

// const i18n = new VueI18n({
//   locale: 'en',
//   messages: i18njson,
//   silentTranslationWarn: true,
// });
const i18n = createI18n({
  locale: 'en',
  messages: i18njson,
  silentTranslationWarn: true
});
// app.config.globalProperties.$i18n = i18n;
app.use(i18n);
app.use(deuFooterHeader, { i18n });

// function createStickyLocale(fallbackLocale = 'en') {
//   return function stickyLocale(to, from, next) {
//     const toLocale = to.query.locale;
//     const fromLocale = from.query.locale;
//
//     const newLocale = toLocale ?? fromLocale ?? fallbackLocale;
//
//     newLocale !== toLocale
//       ? next({ ...to, query: { ...to.query, locale: newLocale } })
//       : next();
//   };
// }
// router.app = app;
app.use(router);
// router.beforeEach(createStickyLocale())
initStore(app);

app.use(UniversalPiwik, {
  router,
  isPiwikPro: app.config.globalProperties.$env.TRACKER_IS_PIWIK_PRO,
  trackerUrl: app.config.globalProperties.$env.TRACKER_TRACKER_URL,
  siteId: app.config.globalProperties.$env.TRACKER_SITE_ID,
  debug: app.config.globalProperties.$env.NODE_ENV === 'development',
});

import 'popper.js';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

/* eslint-disable no-new */
// new Vue({
//   i18n,
//   el: '#app',
//   store,
//   router,
//   components: { App },
//   template: '<App/>'
// });

// router.isReady().then(() => {
//   app.mount('#app');
// });

app.mount('#app');
