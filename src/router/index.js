import { createRouter, createWebHistory } from 'vue-router';
// import vueSmoothScroll from 'vue2-smooth-scroll'

const Dashboard = () => import(/* webpackChunkName: "Dashboard" */'@/components/Dashboard.vue')
const DimensionsDetailsModal = () => import(/* webpackChunkName: "Dashboard" */'@/components/dimensions-cards/DimensionsDetailsModal.vue')

const Catalogues = () => import(/* webpackChunkName: "Catalogues" */'@/components/Catalogues.vue')
const CatalogueDetail = () => import(/* webpackChunkName: "Catalogues" */'@/components/CatalogueDetail.vue')
const CatalogueDetailDashboard = () => import(/* webpackChunkName: "Catalogues" */'@/components/CatalogueDetailDashboard.vue')
const CatalogueDetailDistributions = () => import(/* webpackChunkName: "Catalogues" */'@/components/CatalogueDetailDistributions.vue')
const CatalogueDetailViolations = () => import(/* webpackChunkName: "Catalogues" */'@/components/CatalogueDetailViolations.vue')

const Methodology = () => import(/* webpackChunkName: "Methodology" */'@/components/Methodology.vue')

// Vue.use(vueSmoothScroll)

const rootBreadcrumb = [
  { text: 'Home', to: null, href: '/' },
  { text: 'Metadata Quality Dashboard', to: { name: 'Dashboard' } }
]

export default createRouter({
  history: createWebHistory('/mqa/'),
  linkActiveClass: 'active',
  // scrollBehavior(to, from, savedPosition) {
  //   if (to.matched.some(route => route.meta.scrollTop)) return { left: 0, top: 0 };
  //   else if (savedPosition) return savedPosition;
  //   else return { left: 0, top: 0 };
  // },
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard,
      children: [{
        path: 'dimensions',
        redirect: { name: 'Dashboard' }
      },
      {
        path: 'dimensions/:dimension',
        name: 'dimensions-details',
        component: DimensionsDetailsModal,
        props: {
          dimensionsType: 'dimensions-details'
        }
      }],
      meta: {
        breadcrumbs ({ t }) {
          return [
            ...rootBreadcrumb,
            { text: t.call(this, 'message.navigation.dashboard'), to: { name: 'Dashboard' } }
          ]
        }
      }
    },
    {
      path: '/methodology',
      name: 'Methodology',
      component: Methodology,
      meta: {
        breadcrumbs ({ t }) {
          return [
            ...rootBreadcrumb,
            { text: t.call(this, 'message.navigation.methodology'), to: { name: 'Methodology' } }
          ]
        }
      }
    },
    {
      path: '/catalogues',
      name: 'Catalogues',
      component: Catalogues,
      props: true,
      children: [
        {
          path: ':id',
          components: {
            catalogueDetail: CatalogueDetail
          },
          props: true,
          children: [
            {
              path: '',
              name: 'CatalogueDetailDashboard',
              components: {
                catalogueDetailNavigation: CatalogueDetailDashboard
              },
              children: [{
                path: 'dimensions',
                redirect: { name: 'CatalogueDetailDashboard' }
              },
              {
                path: 'dimensions/:dimension',
                name: 'catalogue-dimensions-details',
                component: DimensionsDetailsModal,
                props: {
                  dimensionsType: 'catalogue-dimensions-details'
                }
              }]
            },
            {
              path: 'distributions',
              name: 'CatalogueDetailDistributions',
              components: {
                catalogueDetailNavigation: CatalogueDetailDistributions
              },
              meta: {
                breadcrumbs ({ t }) {
                  return [
                    { text: t.call(this, 'message.catalogue_detail.distributions.title'), to: { name: 'CatalogueDetailDistributions' } }
                  ]
                }
              }
            },
            {
              path: 'violations',
              name: 'CatalogueDetailViolations',
              components: {
                catalogueDetailNavigation: CatalogueDetailViolations
              },
              meta: {
                breadcrumbs ({ t }) {
                  return [
                    { text: t.call(this, 'message.common.site_title_violations'), to: { name: 'CatalogueDetailViolations' } }
                  ]
                }
              }
            }
          ]
        }
      ],
      meta: {
        breadcrumbs ({ route, store, t }) {
          const catalogTitle = store.getters.getCatalogue && store.getters.getCatalogue.info && store.getters.getCatalogue.info.title
          return [
            ...rootBreadcrumb,
            { text: t.call(this, 'message.navigation.catalogues'), to: { name: 'Catalogues' } },
            ...(route.params.id ? [{ text: catalogTitle, to: { name: 'CatalogueDetailDashboard' } }] : [])
          ]
        }
      }
    }
  ]
})
