/**
 * Configuration template file to bind specific properties from user-config.js to environment variables.
 *
 * This object MUST be structurally identical to the standard user-config.sample.js file.
 * Each value MUST start with the $VITE_ prefix and SHOULD be followed by their path, e.g.
 *
 * api: { baseUrl: '$VITE_API_BASE_URL' }
 *
 * Their corresponding environment variable keys MUST equal that value without the $ character, e.g.
 *
 * VITE_API_BASE_URL: '/base'
 */

export default {
  "NODE_ENV": "$VITE_NODE_ENV",
  "ROOT_API": "$VITE_ROOT_API",
  "ROOT_URL": "$VITE_ROOT_URL",
  "DATA_URL": "$VITE_DATA_URL",
  "TRACKER_IS_PIWIK_PRO": "$VITE_TRACKER_IS_PIWIK_PRO",
  "TRACKER_TRACKER_URL": "$VITE_TRACKER_TRACKER_URL",
  "TRACKER_SITE_ID": "$VITE_TRACKER_SITE_ID",
  "MATOMO_URL": "$VITE_MATOMO_URL",
  "PIWIK_ID": "$VITE_PIWIK_ID",
  "REPORT_URL": "$VITE_REPORT_URL",
  "SHACL_VALIDATOR_URL": "$VITE_SHACL_VALIDATOR_URL",
  "SHACL_API_URL": "$VITE_SHACL_API_URL",
  "DEBUG_DEV": "$VITE_DEBUG_DEV",
  "SCORING_START_DATE": "$VITE_SCORING_START_DATE",
  "SCORING_STEP_SIZE": "$VITE_SCORING_STEP_SIZE",
  "SCORING_MAX_POINTS": "$VITE_SCORING_MAX_POINTS",
  "HISTORY_START_DATE": "$VITE_HISTORY_START_DATE",
  "HISTORY_RESOLUTION": "$VITE_HISTORY_RESOLUTION",
  "SHOW_SPARQL": "$VITE_SHOW_SPARQL"
};
